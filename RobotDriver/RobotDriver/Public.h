#pragma once

/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that app can find the device and talk to it.
//

DEFINE_GUID (GUID_DEVINTERFACE_RobotDriver,
    0x2f6485ce,0xbe50,0x4a78,0xba,0xf6,0xe8,0xfe,0xb1,0x5c,0xa3,0xe4);
// {2f6485ce-be50-4a78-baf6-e8feb15ca3e4}

#define IOCTL_INDEX                     0x800
#define FILE_DEVICE_ROBOTDRIVER         0x8080
#define IOCTL_ROBOTDRIVER_SETGOALPOS CTL_CODE(FILE_DEVICE_ROBOTDRIVER,\
                                                    IOCTL_INDEX + 1, \
                                                    METHOD_BUFFERED, \
                                                    FILE_ANY_ACCESS)