#pragma once

#include "Driver.h"
#include <ntdef.h>

NTSTATUS SetGoalPosition(WDFUSBPIPE pipe, UCHAR id, USHORT position);