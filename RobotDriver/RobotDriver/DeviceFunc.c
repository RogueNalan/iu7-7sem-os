#include "DeviceFunc.h"

NTSTATUS SetGoalPosition(WDFUSBPIPE pipe, UCHAR id, USHORT position)
{
	WDFMEMORY memory;
	PVOID writeBuffer;
	WDF_REQUEST_SEND_OPTIONS sendOptions;
	WDF_MEMORY_DESCRIPTOR memDesc;
	ULONG bytesWritten = 0;

	WDF_REQUEST_SEND_OPTIONS_INIT(&sendOptions, WDF_REQUEST_SEND_OPTION_SEND_AND_FORGET);
	NTSTATUS status = WdfMemoryCreate(
		WDF_NO_OBJECT_ATTRIBUTES,
		NonPagedPool,
		0,
		9,
		&memory,
		&writeBuffer
		);

	if (!NT_SUCCESS(status))
		return status;

	unsigned char* buf = (unsigned char*) writeBuffer;
	buf[0] = 0xFF;
	buf[1] = 0xFF;
	buf[2] = id;
	buf[3] = 0x05; 
	buf[4] = 0x03;
	buf[5] = 0x1E;
	buf[6] = (unsigned char)position;
	buf[7] = (unsigned char)(position << 8);

	unsigned short sum = 0;
	for (int i = 3; i <= 7; ++i)
		sum += buf[i];
	buf[8] = (char)(~sum);

	status = WdfUsbTargetPipeWriteSynchronously
		(
		pipe,
		NULL,
		&sendOptions,
		&memDesc,
		&bytesWritten
		);

	WdfObjectDelete(memory);
	return status;
}