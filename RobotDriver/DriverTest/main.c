#include <stdio.h>
#include <stdlib.h>

#include <windows.h>
#include <SetupAPI.h>

#include "devioctl.h"
#include <initguid.h>
#include "../RobotDriver/Public.h"

HANDLE OpenDev(){

	HDEVINFO							HardwareDeviceInfo;
	SP_DEVICE_INTERFACE_DATA			DeviceInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA	DeviceInterfaceDetailData = NULL;
	LPGUID								InterfaceGuid = (LPGUID)&GUID_DEVINTERFACE_RobotDriver;
	ULONG								Length, RequiredLength = 0, buffersize = 256;

	HardwareDeviceInfo = SetupDiGetClassDevs(
		InterfaceGuid,
		NULL,
		NULL,
		(DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

	if (HardwareDeviceInfo == INVALID_HANDLE_VALUE) {
		printf("SetupDiGetClassDevs failed!\n");
		return INVALID_HANDLE_VALUE;
	}

	DeviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

	DeviceInterfaceDetailData = LocalAlloc(LMEM_FIXED, buffersize + sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA));

	if (!DeviceInterfaceDetailData) {
		SetupDiDestroyDeviceInfoList(HardwareDeviceInfo);
		printf("Failed to allocate memory.\n");
		return INVALID_HANDLE_VALUE;
	}

	DeviceInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

	int count = -1;
	while (SetupDiEnumDeviceInterfaces(HardwareDeviceInfo, 0, InterfaceGuid, ++count, &DeviceInterfaceData));
	HANDLE hd = INVALID_HANDLE_VALUE;
	if (count){
		char *devices = calloc(count * 256, sizeof(char));

		if (!devices) {
			SetupDiDestroyDeviceInfoList(HardwareDeviceInfo);
			printf("Failed to allocate memory.\n");
			return INVALID_HANDLE_VALUE;
		}



		for (int i = 0; SetupDiEnumDeviceInterfaces(HardwareDeviceInfo,
			0,
			InterfaceGuid,
			i,
			&DeviceInterfaceData); i++){



			SetupDiGetDeviceInterfaceDetail(
				HardwareDeviceInfo,
				&DeviceInterfaceData,
				NULL,
				0,
				&RequiredLength,
				NULL
				);

			Length = RequiredLength;
			if (Length > buffersize){
				while (Length > buffersize){
					buffersize <<= 1;
				}
				LocalFree(DeviceInterfaceDetailData);

				DeviceInterfaceDetailData = LocalAlloc(LMEM_FIXED, buffersize + sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA));

				if (!DeviceInterfaceDetailData) {
					SetupDiDestroyDeviceInfoList(HardwareDeviceInfo);
					printf("Failed to allocate memory.\n");
					return INVALID_HANDLE_VALUE;
				}

				DeviceInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			}

			if (!SetupDiGetDeviceInterfaceDetail(
				HardwareDeviceInfo,
				&DeviceInterfaceData,
				DeviceInterfaceDetailData,
				Length,
				&RequiredLength,
				NULL)) {

				printf("Error in SetupDiGetDeviceInterfaceDetail: %d\n", GetLastError());

				SetupDiDestroyDeviceInfoList(HardwareDeviceInfo);
				LocalFree(DeviceInterfaceDetailData);
				free(devices);

				return INVALID_HANDLE_VALUE;
			}

			memcpy(devices + i * 256, DeviceInterfaceDetailData->DevicePath, min(255, Length));
		}
		for (int i = 0; i < count; ++i){
			printf("%d - %s\n", i, devices + i * 256);
		}
		printf("Select device> ");
		int ch;
		do{
			scanf_s("%d", &ch);
		} while (ch < 0 || ch >= count);

		hd = CreateFile(devices + ch * 256,
			GENERIC_WRITE | GENERIC_READ,
			FILE_SHARE_WRITE | FILE_SHARE_READ,
			NULL, // default security
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		free(devices);
	}

	SetupDiDestroyDeviceInfoList(HardwareDeviceInfo);
	LocalFree(DeviceInterfaceDetailData);

	return hd;
}


int __cdecl main()
{
	HANDLE hd = OpenDev();

	int choice = 0;
	if (hd != INVALID_HANDLE_VALUE)
	{
		printf("opened\n");
		do
		{
			unsigned char buffer[3];
			printf("Menu:\n1 - input id and position\n2 - leave");
			scanf("%d", &choice);
			if (choice == 1)
			{
				scanf("%hh %h", &buffer[0], &buffer[1]);
				DeviceIoControl
					(
					hd,
					(DWORD)IOCTL_ROBOTDRIVER_SETGOALPOS,
					buffer,
					2,
					NULL,
					0,
					NULL,
					NULL
					);
			}
		}
		while (choice != 2);
	}
	return 0;
}